package de.mitschwimmer.venomousMinister;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.URI.create;
import static java.net.http.HttpResponse.BodyHandlers.ofString;
import static org.junit.jupiter.api.Assertions.assertEquals;

class WelcomePageTest {

    private final HttpClient client = HttpClient.newHttpClient();
    private final HttpRequest request = HttpRequest.newBuilder().uri(create("http://localhost:8080/")).build();

    @Test
    void getIndexPage() throws IOException, InterruptedException {
        // given
        new Server();

        // when
        var responseCode = client.send(request, ofString()).statusCode();

        // then
        assertEquals(HTTP_OK, responseCode);
    }

}