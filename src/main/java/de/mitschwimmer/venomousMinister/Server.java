package de.mitschwimmer.venomousMinister;

import cloud.piranha.embedded.EmbeddedPiranhaBuilder;
import cloud.piranha.http.impl.DefaultHttpServer;
import cloud.piranha.http.webapp.HttpWebApplicationServerProcessor;
import de.mitschwimmer.jules.Logging;
import gg.jte.ContentType;
import gg.jte.TemplateEngine;

public class Server implements Logging {

    static TemplateEngine templateEngine = TemplateEngine.createPrecompiled(ContentType.Html);

    private final DefaultHttpServer server;

    {
        final var piranha = new EmbeddedPiranhaBuilder()
                .servlet("HelloWorld", RootServlet.class)
                .servletMapping("HelloWorld", "/*")
                .buildAndStart();

        final var processor = new HttpWebApplicationServerProcessor(piranha);
        server = new DefaultHttpServer(8080, processor, false);
        server.start();
    }

    int getPort() {
        return this.server.getServerPort();
    }
}
