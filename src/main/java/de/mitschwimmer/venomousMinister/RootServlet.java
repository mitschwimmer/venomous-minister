package de.mitschwimmer.venomousMinister;

import gg.jte.output.PrintWriterOutput;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import static de.mitschwimmer.venomousMinister.Server.templateEngine;

public class RootServlet extends HttpServlet {
    /**
     * Handle the GET requests.
     *
     * @param request  the request.
     * @param response the response.
     * @throws IOException when an I/O error occurs.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try (final var writer = response.getWriter()) {
            templateEngine.render(
                    "example.jte",
                    new Page("hallo", "titel"),
                    new PrintWriterOutput(writer));
        }
    }
}
