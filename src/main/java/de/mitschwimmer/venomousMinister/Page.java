package de.mitschwimmer.venomousMinister;

public record Page(String description, String title) {
}
