package de.mitschwimmer.venomousMinister;

import de.mitschwimmer.jules.Logging;

public class Main implements Logging {

    public static void main(String[] args) {
        var server = new Server();
        log.info("Started Server on Port: " + server.getPort());
    }
}
