# Venomous Minister
A web application streaming the complete UI to each client browser.

## Stack
An experiment with:

- JTE Templates
- Piranha Cloud Embedded
- Jakarta Servlet 6
- Jakarta Websocket JSR 356

## Goal
Prove that it is enjoyable to build server side apps that depend on a stateful websocket instead of a classic stateless http connection.

## Pushback
I expect the handling of multiple Client states to be too complex to be comfortable.
Building Layouts server side might be troublesome and not straight forward.

## Experiments
- [x] Test on index.html with CSS
- [ ] Send HTML via Websocket
- [ ] Handle two client states on the backend
- [ ] Support client side refresh without loosing state